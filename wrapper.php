<?php get_header( my_template_base() ); ?>
 
			<div id="main" class="site-main row">
			
		  	<div id="primary" class="content-area">
 
					<?php include my_template_path(); ?>
 
		    </div> <!-- end #primary -->

		    <?php get_sidebar( my_template_base() ); ?>

			</div> <!-- end #main -->
 
<?php get_footer( my_template_base() ); ?>