<!doctype html>  

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
	
<head>
	<meta charset="utf-8">
		
	<title><?php wp_title(''); ?></title>
	
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>
	
	<!-- Google Chrome Frame for IE -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<!-- mobile meta (hooray!) -->
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	
	<!-- icons & favicons (for more: http://themble.com/support/adding-icons-favicons/) -->
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
			
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	
	<!-- wordpress head functions -->
	<?php wp_head(); ?>
	<!-- end of wordpress head -->
		
	<!-- drop Google Analytics Here -->
	<!-- end analytics -->
	
</head>
	
	<body <?php body_class(); ?>>

		<div id="page" class="hfeed site">
			
			<header id="masthead" class="site-header" role="banner">
			
				<section class="head_wrap row">

					<div class="logo">
						
						<!-- to use a image just replace the bloginfo('name') with your img src and remove the surrounding <p> -->
						<a href="<?php echo home_url(); ?>" rel="nofollow"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="<?php bloginfo('name'); ?> Home"/></a>
						
						<!-- if you'd like to use the site description you can un-comment it below -->
						<?php // bloginfo('description'); ?>

					</div><!-- .logo -->
						
					<nav class="site-navigation main-nav" role="navigation">
						<?php bones_main_nav(); ?>
					</nav>

				</section>
			
			</header> <!-- end header -->
