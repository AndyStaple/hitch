			<footer id="colophon" class="site-footer" role="contentinfo">

				<div class="site-info row">

					<nav class="foot-nav" role="navigation">
    				<?php bones_footer_links(); ?>
	      	</nav>

	      	<section class="legal">              		
						<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.</p>
					</section>

				</div>
				
			</footer>
		
		<?php wp_footer(); ?>

		</div><!-- #page -->

	</body>

</html>