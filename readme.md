Hitch
====

What is it?
---
Hitch is a Custom WordPress Theme, probably best categorized as a "starter" theme that is customized for use on our WordPress projects.

Features
---
* Utilizes the ThemeWrapper concept (wrapper.php)
* HTML5/CSS3 Structure
* Grid Elements added via sass instead of mucking up html
* ARIA Compliant
* UI elements and Grid from the Foundation Framework
* Basic CSS can be used as a minimalistic theme

How to use this theme
---
To use Hitch, you'll want to understand Sass, as well as install the Compass zurb-foundation Gem. Instructions are available in the [Foundation Docs](http://foundation.zurb.com/docs/compass.php). After that make sure to compile the app.scss file when needed and you'll be ready to roll. More advanced instructions will be added soon.

In the Pipeline
---
* Make it even more lightweight
* Use of HTML5's Figure & Figcaption for WP Captions

Credits
---
* [Zurb Foundation](http://foundation.zurb.com)
* [Bones](http://themble.com/bones/)